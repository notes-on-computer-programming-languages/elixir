# elixir

Functional, concurrent programming language that runs on the Erlang virtual machine (BEAM). [elixir-lang.org](https://elixir-lang.org)

[[_TOC_]]

![popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=rustc+ocaml-base-nox+ghc+julia&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y&beenhere=1)

![popcon](https://qa.debian.org/cgi-bin/popcon-png?packages=julia+scala+racket+elixir+clojure+chezscheme&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y&beenhere=1)

# Presentations
* [*Why Elixir*
  ](https://www.theerlangelist.com/article/why_elixir)
  2014-01 Saša Jurić

# Cheatsheets
* [*Elixir cheatsheet*](https://devhints.io/elixir)

# Tutorials
* [Learn X in Y minutes, where X=elixir](https://learnxinyminutes.com/docs/elixir/)
* [*Elixir School*](https://elixirschool.com)
* [*Elixir (How I Start)*
  ](https://howistart.org/posts/elixir/1/)
  2019-06 José Valim
* [*Elixir: A primer for object-oriented programmers*
  ](https://medium.com/@roydejong/elixir-a-primer-for-object-oriented-programmers-fd5ef0206943)
  2018-08 Roy de Jong

## Currying and Partial Application
* Learn With Me: Elixir - [*Currying and Partial Application (#32)*
  ](https://inquisitivedeveloper.com/lwm-elixir-32/)
  2019-02 Kevin Peter
* [*Creating Curried Functions in Elixir*
  ](https://medium.com/@alexander.s.boring/creating-curried-functions-in-elixir-89bb30de8142)
  2018-02

## Scripts
* [*Writing Shell Scripts Using Elixir*
  ](https://akoutmos.com/post/elixir-shell-scripts/)
  2022-04 Alex Koutmos

# Try it online
* https://tio.run/#elixir

# Books
* [Elixir (Computer program language)
  ](https://www.worldcat.org/search?q=su%3AElixir+%28Computer+program+language%29)
* [*Learning*](http://elixir-lang.org/learning.html)
---
* [*Elixir in Action*
  ](https://www.manning.com/books/elixir-in-action-second-edition)
  Second Edition
  2019 Saša Jurić
* [*Learn Functional Programming with Elixir:
  New Foundations for a New World*
  ](https://www.worldcat.org/search?q=ti:Learn+Functional+Programming+with+Elixir)
  2018-02 Ulisses Almeida
* [*Functional Programming: A PragPub Anthology: Exploring Clojure, Elixir, Haskell, Scala, and Swift*
  ](https://pragprog.com/titles/ppanth/functional-programming-a-pragpub-anthology/)
  2017-07 Michael Swaine and the PragPub writers

# Syntax
## Named arguments
Can we use structs instead of maps for optional named arguments with default values?
* [*Default arguments*
  ](https://elixir-lang.org/getting-started/modules-and-functions.html#default-arguments)
* [Structs](https://elixir-lang.org/getting-started/structs.html)
* [Named parameters](https://rosettacode.org/wiki/Named_parameters#Elixir) Rosetta Code

# Code analysis
* [credo](https://hex.pm/packages/credo)

# Code documentation
* [elixir docstring](https://www.google.com/search?q=elixir+docstring)
* Docstrings are read from .beam files, 
  hence compilation is required before testing doctests.
* [*Writing Documentation*](https://hexdocs.pm/elixir/writing-documentation.html)
* [*Documentation*](https://elixirschool.com/en/lessons/basics/documentation/)
  Elixir School
* [ExUnit.DocTest](https://hexdocs.pm/ex_unit/ExUnit.DocTest.html)

# FFI
* [elixir rust](https://www.google.com/search?q=elixir+rust)
* [rustler](https://hex.pm/packages/rustler)
* [*Writing Rust NIFs for Elixir With Rustler*
  ](https://simplabs.com/blog/2020/06/25/writing-rust-nifs-for-elixir-with-rustler/)
  2020-06 Niklas Long
  * v0.22 release will provide a much cleaner syntax

# Socket
* [elixir socket](https://google.com/search?q=elixir+socket)
* [*Task and gen_tcp*
  ](https://elixir-lang.org/getting-started/mix-otp/task-and-gen-tcp.html)
* [elixir ranch](https://google.com/search?q=elixir+ranch)
* [*Running Phoenix on a Unix socket*
  ](https://elixirforum.com/t/running-phoenix-on-a-unix-socket/9703)
* "With :gen_tcp you can use the tuple {:local, path} to connect to a unix socket."
* Erlang: http://erlang.org/doc/man/inet.html#type-local_address
* [*Connect to TCP clients Elixir*
  ](https://stackoverflow.com/questions/43597653/connect-to-tcp-clients-elixir)
  (2017) stack*overflow*
* ([*Unix Domain Sockets in Elixir*
  ](https://stackoverflow.com/questions/34711738/unix-domain-sockets-in-elixir))
* [*Handling TCP connections in Elixir*
  ](https://andrealeopardi.com/posts/handling-tcp-connections-in-elixir/)
  2015-06 Andrea Leopardi

# Web frameworks and libraries
* [*Phoenix (web framework)*](https://en.m.wikipedia.org/wiki/Phoenix_(web_framework))
* [trot](https://hex.pm/packages/trot)
* [*Building a web framework from scratch in Elixir*
  ](https://codewords.recurse.com/issues/five/building-a-web-framework-from-scratch-in-elixir)
  Robert Lord
* Missing FastCGI server library...

# Static site generators
* [*A simple static site generator in Elixir*
  ](https://cjkinni.com/words/2021-04-09-a-simple-static-site-generator-in-elixir.html)
  2021-04 cjkinni
* still, serum, obelisk

# Paginator
* [elixir paginator](https://google.com/search?q=elixir+paginator)
* [duffelhq/paginator](https://github.com/duffelhq/paginator)

# Ecto: interact with databases
* [ecto](https://hex.pm/packages/ecto)
* ecto SQL
  * AyeSQL: raw SQL in ecto
  * [*Cursor Pagination & Ecto*](https://elixirforum.com/t/cursor-pagination-ecto/13517)
  * ecto_cursor_pagination
  * paginator
* ecto mnesia

# NoSQL Databases
## Mnesia (from OTP)
* [*mnesia*](https://erlang.org/doc/man/mnesia.html)
* [*Mnesia*](https://elixirschool.com/en/lessons/specifics/mnesia/) (Elixir School)
* Chapter 20 Mnesia: The Erlang Database
  [Programming Erlang](http://www.worldcat.org/oclc/900320463)
* (fr) [*Mnesia : la base de données intégrée à Erlang*
  ](https://blog.derniercri.io/mnesia-la-base-de-donnees-integree-a-erlang/)
  2016-10 Xavier Van de Woestyne

## ExBitcask
* [*Bitcask*](https://en.m.wikipedia.org/wiki/Bitcask) (Wikipedia)
* [ExBitcask](https://hexdocs.pm/ex_bitcask/)

# Key-value data store: ETS and DETS (from OTP)
* [ets](https://hex.pm/packages/ets)
* [*Erlang Term Storage (ETS)*](https://elixirschool.com/en/lessons/specifics/ets/)
* [*Elixir ETS Versus Redis*](https://blog.codeship.com/elixir-ets-vs-redis/)
  2017-05 Barry Jones

# (Other) Libraries key-value data store
# GUI
* [*[erlang-questions] My quest for a decent way to make a GUI in erlang*
  ](http://erlang.org/pipermail/erlang-questions/2017-July/092949.html)
  2017-07 Joe Armstrong

# Multimedia
## Membrane
* [membrane_core](https://hex.pm/packages/membrane_core)
* [Membrane Framework](https://codesync.global/uploads/media/activity_slides/0001/01/14963e131e6a0b361cb2e39c757053c3eaf5e4e5.pdf)
  2019

# Functional programming
* [monad](https://hex.pm/packages?search=monad&sort=recent_downloads) keyword on Hex.
* ok Elegant error/exception handling in Elixir, with result monads.
* towel A simple Maybe monad for Elixir.
* [witchcraft](https://hex.pm/packages/witchcraft)
  * Do-notation (?) documentation in [algae](https://hex.pm/packages/algae) or (unofficial) [Witchcraft Monadic Do Notation Primer](https://gist.github.com/expede/59b4e7e49fd394210ca5f31c7f00f382)
  * [*Monads and other dark magic for Elixir*
    ](https://laptrinhx.com/monads-and-other-dark-magic-for-elixir-2521426524/)
    2020-02
* ...
* [monad](https://hex.pm/packages/monad) Monads and do-syntax for Elixir
  * [*Monad behaviour*
    ](https://hexdocs.pm/monad/Monad.html)
  * old, inspired witchcraft

## Chaining Function Calls
* [*Chaining Function Calls in Elixir*
  ](http://www.tobysteele.com/2018/05/04/chaining-function-calls-in-elixir/)
  2018-05 Toby

# Dependency injection
* [elixir dependency injection](https://google.com/search?q=elixir+dependency+injection)
* [*Lightweight dependency injection in Elixir (without the tears)*
  ](https://blog.carbonfive.com/lightweight-dependency-injection-in-elixir-without-the-tears/)
  2018-03 Andrew Hao
* [*How to use dependency injection pattern in Elixir?*
  ](https://elixirforum.com/t/how-to-use-dependency-injection-pattern-in-elixir/3202)
* [*Dependency Injection in Elixir is a Beautiful Thing*
  ](https://www.openmymind.net/Dependency-Injection-In-Elixir/)
  2017-01 

# Metaprogramming
* [*An Introduction to Metaprogramming in Elixir*
  ](https://blog.appsignal.com/2021/09/07/an-introduction-to-metaprogramming-in-elixir.html)
  2021-09 Jia Hao Woo

# Indexes
* [The package manager for the Erlang ecosystem](https://hex.pm)
* https://github.com/h4cc/awesome-elixir
  * #static-page-generation

# Science papers
* [elixir programming](https://scholar.google.ca/scholar?q=elixir+programming)

# Comparing Elixir with other languages
* [*Elixir Review*
  ](https://www.slant.co/options/1540/~elixir-review)
  Slant
* [*What are the best functional programming languages for programming beginners?*
  ](https://www.slant.co/topics/708/~best-functional-programming-languages-for-programming-beginners)
  Slant
---
* [*Elixir vs F# – opinionated syntax comparison*
  ](https://mlusiak.com/elixir-vs-fsharp/)
  2017-03 Michał Łusiak
* [*A Language for the Next 10 Years*
  ](https://programmingzen.com/next-programming-language/)
  2016-06 Antonio Cangiano
