defmodule EmptyFrame do
  @behaviour :wx_object
  use Bitwise

  @title 'Elixir OpenGL'
  @size {600, 600}

  @wxID_ANY -1

  #######
  # API #
  #######
  def start_link() do
    :wx_object.start_link(__MODULE__, [], [])
  end
  # EmptyFrame.start_link

  #################################
  # :wx_object behavior callbacks #
  #################################
  def init(config) do
    wx = :wx.new(config)
    frame = :wxFrame.new(wx, @wxID_ANY, @title, [{:size, @size}])
    :wxWindow.connect(frame, :close_window)
    :wxFrame.show(frame)
    opts = [{:size, @size}]
    {frame, %{}}
  end

  def code_change(_, _, state) do
    {:stop, :not_implemented, state}
  end

  def handle_cast(msg, state) do
    IO.puts "Cast:"
    IO.inspect msg
    {:noreply, state}
  end

  def handle_call(msg, _from, state) do
    IO.puts "Call:"
    IO.inspect msg
    {:reply, :ok, state}
  end

  def handle_info(:stop, state) do
    {:stop, :normal, state}
  end

  def handle_info(:update, state) do
    :wx.batch(fn -> render(state) end)
    {:noreply, state}
  end

  def handle_event({:wx, _, _, _, {:wxClose, :close_window}}, state) do
    {:stop, :normal, state}
  end

  def handle_event({:wx, _, _, _, {:wxSize, :size, {width, height}, _}}, state) do
    {:noreply, state}
  end

  def terminate(_reason, state) do
  end

  #####################
  # Private Functions #
  #####################
  defp render(%{canvas: canvas} = _state) do
    :ok
  end

end

# wxErlang / wx_object
# https://erlang.org/doc/man/wx_object.html

# Getting started with OpenGL in Elixir
# https://wtfleming.github.io/2016/01/06/getting-started-opengl-elixir/
