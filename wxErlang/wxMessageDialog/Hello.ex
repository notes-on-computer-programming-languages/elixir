:wx.new()

hello_dialog = :wxMessageDialog.new(:wx.null(), "Hello World")

# new(Parent, Message) -> wxMessageDialog()
# Types
# Parent = wxWindow:wxWindow()
# Message = unicode:chardata()

:wxMessageDialog.showModal(hello_dialog)

# https://arifishaq.files.wordpress.com/2017/12/wxerlang-getting-started.pdf